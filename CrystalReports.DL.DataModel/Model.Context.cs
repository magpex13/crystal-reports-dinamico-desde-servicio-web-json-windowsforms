﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CrystalReports.DL.DataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ONPEEntities : DbContext
    {
        public ONPEEntities()
            : base("name=ONPEEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Candidato> Candidato { get; set; }
        public DbSet<Distrito> Distrito { get; set; }
        public DbSet<PartidoPolitico> PartidoPolitico { get; set; }
    }
}
