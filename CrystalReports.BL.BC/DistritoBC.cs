﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrystalReports.DL.DataModel;

namespace CrystalReports.BL.BC
{
    public class DistritoBC
    {
        public List<Distrito> ListaDeDistritos()
        {
            ONPEEntities context = new ONPEEntities();
            return context.Distrito.ToList();
        }
    }
}
