﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalReports.DL.DataModel;
using CrystalReports.BL.BC;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.IO;
using System.Net.Mail;

namespace CrystalReports.UI
{
    public partial class Form1 : Form
    {
        private char caracterPorDefecto;
        private string direccionReportePDF;

        public Form1()
        {
            InitializeComponent();

            caracterPorDefecto = '/';
            CustomersReport reportDoc = new CustomersReport();

            Customers objCustomers = getDataEntityDB(reportDoc);
            if(objCustomers!=null)
                reportDoc.SetDataSource(objCustomers);

            crystalReportViewer1.ReportSource = reportDoc;
            crystalReportViewer1.Refresh();

            direccionReportePDF = "G:/csharp.net-informations.pdf";//Direccion donde se guardara el pdf
            ExportarAPDF(reportDoc);
            //PrintReport("max", reportDoc);
            //EnviarPorCorreo("RemitenteCorreo", "DestinatarioCorreo", "RemitenteContrasena");
        }

        public Customers getDataEntityDB(ReportDocument objReportDoc)
        {
            try
            {
                Customers ds = new Customers();
                JsonParserCrack crack = new JsonParserCrack();

                var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/TramiteDocumentario.svc/json/tramitedetalle/0000574748");
                // var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/Usuario.svc/json/login/2/0/0/08225169");
                // var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/Tupa.svc/json/derechopago/007/006");
                // var dic = crack.parsearDataJSONdesdeURL(" http://www.munisanisidro.gob.pe/Servicios/TramiteDocumentario.svc/json/tramitedetalle/0000574748");
                //var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/Usuario.svc/json/login/1/01068013/S4O3G0/0");

                var colNames = dic.Keys.ToList();

                //Se setean los nombres de las columanas del CustomersReport.rpt
                List<TextObject> headerReporte = objReportDoc.ReportDefinition.Sections["Section1"].ReportObjects.OfType<TextObject>().ToList();
                List<TextObject> lstTextObjects = objReportDoc.ReportDefinition.Sections["Section2"].ReportObjects.OfType<TextObject>().ToList();

                headerReporte[0].Text += " " + crack.titulo;

                for (int i = 0; i < lstTextObjects.Count; i++)
                {
                    lstTextObjects[i].ObjectFormat.EnableCanGrow = true;

                    if (colNames.Count > i)
                        lstTextObjects[i].Text = colNames[i];
                    else
                        lstTextObjects[i].Text = string.Empty;
                }

                //Se llenan los datos
                DataRow dr;
                var valores = dic[colNames[0]];

                for (int i = 0; i < valores.Count; i++)
                {
                    dr = ds.Tables[0].Rows.Add();
                    for (int j = 0; j < colNames.Count; j++)
                    {
                        dr[j] = dic[colNames[j]][i];
                    }
                }

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un Error");
                return null;
            }
        }

        private void ExportarAPDF(ReportDocument cryRpt)
        {
            try
            {
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = direccionReportePDF; 
                CrExportOptions = cryRpt.ExportOptions;
                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                CrExportOptions.FormatOptions = CrFormatTypeOptions;
                cryRpt.Export();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void EnviarPorCorreo(string de,string para,string contrasena)
        {
            try
            {
                var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential(de, contrasena),
                    EnableSsl = true
                };

                MailMessage message = new MailMessage(de, para, "Reporte PDF Crystal", "Crackk");
                message.Attachments.Add(new Attachment(direccionReportePDF));
                client.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
           
        }

        private void PrintReport(string printerName, ReportDocument Report)
        {
            PageMargins margins;

            margins = Report.PrintOptions.PageMargins;
            margins.bottomMargin = 350;
            margins.leftMargin = 350;
            margins.rightMargin = 350;
            margins.topMargin = 350;
            Report.PrintOptions.ApplyPageMargins(margins);

            Report.PrintToPrinter(1, false, 0, 0);
        }

    }
}
